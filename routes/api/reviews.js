const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');

// Review Model
const Review = require('../../models/Review');

// @route   GET api/reviews
// @desc    Get All reviews
// @access  Public
router.get('/', (req, res) => {
  //Review.find({id_reviewer:'eggs'})
  Review.find()
    .sort({ date: -1 })
    .then(reviews => res.json(reviews));
});

// @route   POST api/reviews
// @desc    Create An Review
// @access  Private
router.post('/', auth, (req, res) => {
    const newReview = new Review({
      id_reviewer: req.body.id_reviewer,
      id_university_reviewed: req.body.id_university_reviewed,

      startMobility: req.body.startMobility,
      endMobility: req.body.endMobility,

      university_Q1: req.body.university_Q1,
      university_Q2: req.body.university_Q2,
      university_Q3: req.body.university_Q3,
      university_Q4: req.body.university_Q4,
      university_QE: req.body.university_QE,

      faculty_Q1: req.body.faculty_Q1,
      faculty_Q2: req.body.faculty_Q2,
      faculty_Q3: req.body.faculty_Q3,
      faculty_QE: req.body.faculty_QE,

      city_Q1: req.body.city_Q1,
      city_Q2: req.body.city_Q2,
      city_Q3: req.body.city_Q3,
      city_Q4: req.body.city_Q4,
      city_QE: req.body.city_QE,

      housing_Q1: req.body.housing_Q1,
      housing_Q2: req.body.housing_Q2,
      housing_Q3: req.body.housing_Q3,
      housing_Q4: req.body.housing_Q4,
      housing_Q5: req.body.housing_Q5,
      housing_Q6: req.body.housing_Q6,
      housing_QE: req.body.housing_QE
    });
  newReview.save().then(review => res.json(review));
  
});

// @route   DELETE api/reviews/:id
// @desc    Delete A Review
// @access  Private
router.delete('/:id', auth, (req, res) => {
  Review.findById(req.params.id)
    .then(review => review.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }));
});

module.exports = router;
