const express = require('express');
const router = express.Router();

// University Model
const University = require('../../models/University');

// @route   GET api/universities
// @desc    Get All universities
// @access  Public
router.get('/', (req, res) => {
  University.find()
    .sort({ name: -1 })
    .then(universities => res.json(universities));
});

// @route   POST api/universities
// @desc    Create An university
// @access  Private
router.post('/', (req, res) => {
  const newUniversity = new University({
    name: req.body.name,
    country: req.body.country,
    city: req.body.city,
    address: req.body.address,
    telephone: req.body.telephone,
    website: req.body.website,
    email: req.body.email,
    emailDomain: req.body.emailDomain
  });

  newUniversity.save().then(university => res.json(university));
});

// @route   DELETE api/universities/:id
// @desc    Delete A University
// @access  Private
router.delete('/:id', (req, res) => {
  University.findById(req.params.id)
    .then(university => university.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }));
});

module.exports = router;
