const express = require('express');
const router = express.Router();

// Comment Model
const Comment = require('../../models/Comment');

// @route   GET api/comments
// @desc    Get All comments
// @access  Public
router.get('/', (req, res) => {
  Comment.find()
    .then(comments => res.json(comments));
});

// @route   POST api/comments
// @desc    Create A comment
// @access  Private
router.post('/', (req, res) => {
  const newComment = new Comment({
    id_user: req.body.id_user,
    id_user_cod : req.body.id_user_cod,
    id_review: req.body.id_review,
    content: req.body.content,
  });

  newComment.save().then(comment => res.json(comment));
});

// @route   DELETE api/comments/:id
// @desc    Delete A comment
// @access  Private
router.delete('/:id', (req, res) => {
  Comment.findById(req.params.id)
    .then(comment => comment.remove().then(() => res.json({ success: true })))
    .catch(err => res.status(404).json({ success: false }));
});

module.exports = router;
