const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const CommentSchema = new Schema({
  id_user: {
    type: String,
    required: true
  },

  id_user_cod: {
    type: String,
    required: true
  },

  id_review: {
    type: String,
    required: true
  },

  content: {
    type: String,
    required: true
  },

  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Comment = mongoose.model('comment', CommentSchema);
