const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const ReviewSchema = new Schema({

  id_reviewer: {
    type: String,
    required: true
  },

  id_university_reviewed: {
    type: String,
    required: true
  },

  startMobility: {
    type: Date,
    required: true
  },

  endMobility: {
    type: Date,
    required: true
  },
  
  date: {
    type: Date,
    default: Date.now
  },

  university_Q1: {
    type: Number,
    required: true
  },

  university_Q2: {
    type: Number,
    required: true
  },

  university_Q3: {
    type: Number,
    required: true
  },

  university_Q4: {
    type: Number,
    required: true
  },

  university_QE: {
    type: String,
    required: false
  },

  faculty_Q1: {
    type: Number,
    required: true
  },

    
  faculty_Q2: {
    type: Number,
    required: true
  },


  faculty_Q3: {
    type: Number,
    required: true
  },

  faculty_QE: {
    type: String,
    required: false
  },
  
  city_Q1: {
    type: Number,
    required: true
  },

  city_Q2: {
    type: Number,
    required: true
  },

  city_Q3: {
    type: Number,
    required: true
  },
  city_Q4: {
    type: Number,
    required: true
  },

  city_QE: {
    type: String,
    required: false
  },
  
  housing_Q1: {
    type: Number,
    required: true
  },

  housing_Q2: {
    type: String,
    required: true
  },

  housing_Q3: {
    type: Number,
    required: true
  },

  housing_Q4: {
    type: Number,
    required: false
  },

  housing_Q5: {
    type: String,
    required: false
  },

  housing_Q6: {
    type: Number,
    required: false
  },

  housing_QE: {
    type: String,
    required: false
  }

});

module.exports = Review = mongoose.model('review', ReviewSchema);
