const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const UniversitySchema = new Schema({
  name: {
    type: String,
    required: true
  },

  country: {
    type: String,
    required: true
  },

  city: {
    type: String,
    required: true
  },

  address: {
    type: String,
    required: true
  },

  telephone: {
    type: String,
    required: true
  },

  website: {
    type: String,
    required: true
  },

  email: {
    type: String,
    required: true
  },
  
  emailDomain: {
    type: String,
    required: true
  },

  date: {
    type: Date,
    default: Date.now
  },
  
});

module.exports = University = mongoose.model('university', UniversitySchema);
