**# LaboratorioDiProgettazione2018-2019**

**Project:** "ErasmusGo"

**Members:**
- Prete Francesco 793389
- Pennati Mattia 793375
- Spreafico Andrea 793317
- Rima Mirko 793435

**Download, Install and Execute**

- **Download**

You can directly locally clone the Gitlab repository:

`git clone https://gitlab.com/Eldritchhh/laboratoriodiprogettazione2018-2019.git`

Otherwise, if you want to download the zip folder, just navigate to the following url and click on the download button:

`https://gitlab.com/Eldritchhh/laboratoriodiprogettazione2018-2019`

- **Install**

Once you've correctly downloaded it, just open a commands shell within the main folder "laboratoriodiprogettazione2018-2019".

Now you just type the following command to install all the required dependencies:

`npm install`

Once all the dependencies have been correctly installed, just move in the "client" folder and execute the same command:

`cd client`

`npm install`

- **Execute**

Now you should be able to execute the code. To run the code, open a commands shell within the main folder "laboratoriodiprogettazione2018-2019".

You can run the code with the following commands:

- `npm run server`: in order to run just the server. It will be reachable in your browser at the port 5000 (localhost:5000)
- `npm run client`: in order to run just the client. It will be reachable in your browser at the port 3000 (localhost:3000)
- `npm run dev`: in order to run both the server and client sides.
- `npm run test`: in order to run and check the test results.

- **Deployment URL**

A working version of the application has been already successfully deployed online, even if it’s still a demo version. The hosting URL is the following: 

- `https://agile-beach-85809.herokuapp.com`

**Admin credentials**

Username: admin@campus.unimib.it

Password: adminadmin

