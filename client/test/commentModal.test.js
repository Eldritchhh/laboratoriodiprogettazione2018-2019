
const commentValidation = require('./../src/validationForms/CommentValidation');

test('Test #1: Normal case - All the fields have been correctly filled!', () => {
    
    newComment = {id_user: "test_id_user", id_review: "test_id_review", content: "wow, bella recensione", date: "01/01/2016" };

  expect(commentValidation(newComment)).not.toContain("You must enter a content for the current comment!");
});

test('Test #2: Error case - Check if all the required fields have been entered!', () => {
    
    newComment = {id_user: "test_id_user", id_review: "test_id_review", content: "", date: "01/01/2016" };

  expect(commentValidation(newComment)).toContain("You must enter a content for the current comment!");
});

