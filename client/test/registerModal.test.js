
const registerValidation = require('./../src/validationForms/RegisterValidation');

test('Test #1: Check if all the required fields have been entered!', () => {
    newUser = {
        name : "Matteo Pipolo",
        email : "m1.p1@campus.unimib.it",
        department : "Informatica",
        password : "test_pswd"
      }  
    expect(registerValidation(newUser)).not.toContain("Please enter all the above fields!");
});

test('Test #2: Check if the password is at least 8 chars!', () => {
  newUser = {
    name : "Matteo Pipolo",
    email : "m2.p2@campus.unimib.it",
    department : "Informatica",
    password : "test_pwd"
    }  
  expect(registerValidation(newUser)).not.toContain("Password has to be at least of 8 chars!");
});

test('Test #3: Error case - Empty username field!', () => {
  newUser = {
    name : "",
    email : "m3.p3@campus.unimib.it",
    department : "Informatica",
    password : "test_psw"
    }  
  expect(registerValidation(newUser)).toContain("Please enter all the above fields!");
});


test('Test #4: Error case - Email not belonging to the Unimib domain!', () => {
  newUser = {
    name : "Matteo Pipolo",
    email : "m4.p4@hotmail.it",
    department : "Informatica",
    password : "test_psw"
    }  
  expect(registerValidation(newUser)).toContain("Email must belong to Unimib email domain!");
});


test('Test #5: Error case - User name already exists!', () => {
  newUser = {
    name : "Matteo Pipolo",
    email : "m5.p5@campus.unimib.it",
    department : "Informatica",
    password : "test_psw"
    }  
  //expect(registerValidation(newUser)).toContain();
  //("User already exists!")
  
});

test('Test #6: Error case - Email field unvalid enter!', () => {
  newUser = {
    name : "Matteo Pipolo",
    email : "m6.p6@campus",
    department : "Informatica",
    password : "test_psw"
    }  
  expect(registerValidation(newUser)).toContain("Email field enter is not valid!");
});

test('Test #7: Error case - User name field cannot contains special chars!', () => {
  newUser = {
    name : "Matt3o P!pol$",
    email : "m7.p7@campus.unimib.it",
    department : "Informatica",
    password : "test_psw"
    }  
  expect(registerValidation(newUser)).toContain("User name field cannot contains special chars!");
});

test('Test #8: Error case - Password field must contains at least 1 special char!', () => {
  newUser = {
    name : "Matteo Pipolo",
    email : "m8.p8@campus.unimib.it",
    department : "Informatica",
    password : "testpswerr"
    }  
  expect(registerValidation(newUser)).toContain("Password field must contains at least 1 special char!");
});