
const formValidation = require('./../src/validationForms/ReviewValidation');

test('Test #1: Normal case - All the fields have been correctly filled!', () => {
  newReview = {startMobility: '01/01/16', endMobility: '01/06/16',  id_reviewer: 123456789,  id_university_reviewed: 123456789,
  university_Q1: 5,  university_Q2: 1,  university_Q3: 5,  university_Q4: 4,  university_QE: "wow",
  faculty_Q1: 5,  faculty_Q2: 3,  faculty_Q3: 3,  faculty_QE: "great",
  city_Q1: 4,  city_Q2: 4,  city_Q3: 3,  city_Q4: 4,  city_QE: "good pizza with pepperoni",
  housing_Q1: 2, housing_Q2: 3,  housing_Q3: 1,  housing_Q4: "",  housing_Q5: 4,  housing_Q6: 5,  housing_QE: "bad, expensive"
  };

  expect(formValidation(newReview)).not.toContain("Please enter all the 'University' fields");
  expect(formValidation(newReview)).not.toContain("Please enter all the 'Housing' fields");
  expect(formValidation(newReview)).not.toContain("Please enter all the 'Faculty' fields");
  expect(formValidation(newReview)).not.toContain("Please enter all the 'City' fields");
  
});

test('Test #2: Limit case - All the fields have been correctly filled with all textual field empty!', () => {
  newReview = {startMobility: '01/01/16', endMobility: '01/06/16',  id_reviewer: 123456789,  id_university_reviewed: 123456789,
  university_Q1: 5,  university_Q2: 1,  university_Q3: 5,  university_Q4: 4,  university_QE: "",
  faculty_Q1: 5,  faculty_Q2: 3,  faculty_Q3: 3,  faculty_QE: "",
  city_Q1: 4,  city_Q2: 4,  city_Q3: 3,  city_Q4: 4,  city_QE: "",
  housing_Q1: 2, housing_Q2: 3,  housing_Q3: 1,  housing_Q4: "",  housing_Q5: 4,  housing_Q6: 5,  housing_QE: ""
  };

  expect(formValidation(newReview)).not.toContain("Please enter all the 'University' fields");
  expect(formValidation(newReview)).not.toContain("Please enter all the 'Housing' fields");
  expect(formValidation(newReview)).not.toContain("Please enter all the 'Faculty' fields");
  expect(formValidation(newReview)).not.toContain("Please enter all the 'City' fields");
  
});

test('Test #3: Error case - Check if all the required fields have been entered!', () => {
  newReview = {startMobility: '01/01/16', endMobility: '01/06/16',  id_reviewer: 123456789,  id_university_reviewed: 123456789,
  university_Q1: 5,  university_Q2: 0,  university_Q3: 5,  university_Q4: 4,  university_QE: "wow",
  faculty_Q1: 5,  faculty_Q2: 0,  faculty_Q3: 3,  faculty_QE: "great",
  city_Q1: 4,  city_Q2: 4,  city_Q3: 3,  city_Q4: 4,  city_QE: "good pizza with pepperoni",
  housing_Q1: 2, housing_Q2: 3,  housing_Q3: 0,  housing_Q4: "",  housing_Q5: 4,  housing_Q6: 5,  housing_QE: "bad, expensive"
  };

  expect(formValidation(newReview)).toContain("Please enter all the 'University' fields");
  expect(formValidation(newReview)).toContain("Please enter all the 'Housing' fields");
  expect(formValidation(newReview)).toContain("Please enter all the 'Faculty' fields");
});

test('Test #4: Check if the start date is before the end date!', () => {
  newReview = {startMobility: '06/15/16', endMobility: '03/15/16',  id_reviewer: 123456789,  id_university_reviewed: 123456789,
  university_Q1: 5,  university_Q2: 2,  university_Q3: 5,  university_Q4: 4,  university_QE: "wow",
  faculty_Q1: 5,  faculty_Q2: 1,  faculty_Q3: 3,  faculty_QE: "great",
  city_Q1: 4,  city_Q2: 4,  city_Q3: 3,  city_Q4: 4,  city_QE: "good pizza with pepperoni",
  housing_Q1: 2, housing_Q2: 3,  housing_Q3: 3,  housing_Q4: "",  housing_Q5: 4,  housing_Q6: 5,  housing_QE: "bad, expensive"
  };

  expect(formValidation(newReview)).toContain("The ending date can't be before the starting date of your mobility");
});

test('Test #5: Check if the mobility period is shorter than 3 month!', () => {
  newReview = {startMobility: '01/31/16', endMobility: '02/28/16',  id_reviewer: 123456789,  id_university_reviewed: 123456789,
  university_Q1: 5,  university_Q2: 2,  university_Q3: 5,  university_Q4: 4,  university_QE: "wow",
  faculty_Q1: 5,  faculty_Q2: 1,  faculty_Q3: 3,  faculty_QE: "great",
  city_Q1: 4,  city_Q2: 4,  city_Q3: 3,  city_Q4: 4,  city_QE: "good pizza with pepperoni",
  housing_Q1: 2, housing_Q2: 3,  housing_Q3: 3,  housing_Q4: "",  housing_Q5: 4,  housing_Q6: 5,  housing_QE: "bad, expensive"
  };

  expect(formValidation(newReview)).toContain("A valid mobility period can't be shorter than 3 month. Please enter valid mobility dates");
});

test('Test #6: Check if the mobility period is longer than 12 month!', () => {
  newReview = {startMobility: '01/31/16', endMobility: '02/28/18',  id_reviewer: 123456789,  id_university_reviewed: 123456789,
  university_Q1: 5,  university_Q2: 2,  university_Q3: 5,  university_Q4: 4,  university_QE: "wow",
  faculty_Q1: 5,  faculty_Q2: 1,  faculty_Q3: 3,  faculty_QE: "great",
  city_Q1: 4,  city_Q2: 4,  city_Q3: 3,  city_Q4: 4,  city_QE: "good pizza with pepperoni",
  housing_Q1: 2, housing_Q2: 3,  housing_Q3: 3,  housing_Q4: "",  housing_Q5: 4,  housing_Q6: 5,  housing_QE: "bad, expensive"
  };

  expect(formValidation(newReview)).toContain("A valid mobility period can't be longer than 12 month. Please enter valid mobility dates");
});