

const UniversityValidation = require('./../src/validationForms/UniversityValidation');


test('Test #1: Normal case - Search for a university by keyword and without filters!', () => {

  searchByName = "Bicocca"
  searchByCountry = ""
  searchByCity = ""
  universities_number = 3

  expect(UniversityValidation(searchByName, searchByCountry, searchByCity, universities_number)).not.toContain("Unviersity name cannot be longer than 60 chars!");

});

test('Test #2: Normal case - Search for a university by full name and one filter!', () => {
  
  searchByName = "University of Milan"
  searchByCountry = ""
  searchByCity = "Milan"
  universities_number = 3

  expect(UniversityValidation(searchByName, searchByCountry, searchByCity, universities_number)).not.toContain("Unviersity name cannot be longer than 60 chars!");

});

test('Test #3: Limit case - Search for a university name that doesnt exists in the DB!', () => {
  
  searchByName = "‘Colosseo’"
  searchByCountry = ""
  searchByCity = ""
  universities_number = 3

  expect(UniversityValidation(searchByName, searchByCountry, searchByCity, universities_number)).not.toContain("Unviersity name cannot be longer than 60 chars!");
});

test('Test #4: Error case - Search for a university name that is longer than 60 chars!', () => {
  
  searchByName = "Bicoccaaaaaaaaaaaaaaaaaaaamirkoaaaaaaaaaaaarivaaaaaaaaaaaaaaaaa"
  searchByCountry = ""
  searchByCity = ""
  universities_number = 3

  expect(UniversityValidation(searchByName, searchByCountry, searchByCity, universities_number)).toContain("Unviersity name cannot be longer than 60 chars!");

});

test('Test #5: Limit case - Search for a university by keyword and filter!', () => {
  
  searchByName = "Uni"
  searchByCountry = ""
  searchByCity = ""
  universities_number = 3

  expect(UniversityValidation(searchByName, searchByCountry, searchByCity, universities_number)).not.toContain("Unviersity name cannot be longer than 60 chars!");

});

test('Test #6: Limit case - Search for a university without any filters!', () => {
  
  searchByName = "Uni"
  searchByCountry = "Italy"
  searchByCity = "Milan"
  universities_number = 3
  
  expect(UniversityValidation(searchByName, searchByCountry, searchByCity, universities_number)).not.toContain("Unviersity name cannot be longer than 60 chars!");

});

test('Test #7: Limit case - Search for a university without keyword but with 2 filter!', () => {

  searchByName = ""
  searchByCountry = "Italy"
  searchByCity = "Milan"
  universities_number = 3
  
  expect(UniversityValidation(searchByName, searchByCountry, searchByCity, universities_number)).not.toContain("Unviersity name cannot be longer than 60 chars!");

});
