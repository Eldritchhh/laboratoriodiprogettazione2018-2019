import React, { Component } from 'react';
import { connect } from 'react-redux';
import UniversityGrid from '../components/university/UniversityGrid';

class Home extends Component {
    render() {

      return (
        <UniversityGrid />
      );
    }
  }
  
  const mapStateToProps = state => ({
    auth: state.auth
  });
  
  export default connect(
    mapStateToProps,
    null
  )(Home);
  