import React, { Component } from 'react';
import {  Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { getUsers, deleteUser } from '../actions/userActions';
import UserList from '../components/auth/UserList';
import { getUniversities, deleteUniversity } from '../actions/universityActions';
import UniversityList from '../components/university/UniversityList';
import { getReviews, deleteReview } from '../actions/reviewActions';
import ReviewList from '../components/review/ReviewList';
import { getComments, deleteComment } from '../actions/commentActions';
import CommentList from '../components/review/CommentList';
import PropTypes from 'prop-types';

class AdminDashboard extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    const { isAuthenticated, user } = this.props.auth;

    return (
      <div>
        {isAuthenticated? (
        <div>
        {user.admin === true ? (
       <div>
       {/* Displaying the navigation bar and the available tabs 
            Tab 1: Description of the university
            Tab 2: List of the reviews
            Tab 3: Statistics about the university */}
            <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '1' })}
                onClick={() => { this.toggle('1'); }}
              >
                Universities
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '2' })}
                onClick={() => { this.toggle('2'); }}
              >
                Users
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '3' })}
                onClick={() => { this.toggle('3'); }}
              >
                Reviews
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '4' })}
                onClick={() => { this.toggle('4'); }}
              >
                Comments
              </NavLink>
            </NavItem>
          </Nav>

       {/* Displaying the TAB 1: List of all the universities */}
       <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <div style={{marginTop: "2rem"}}>
               <UniversityList/>
            </div>
          </TabPane>

          {/* Displaying the TAB 2: List of all the users */}
          <TabPane tabId="2">
          <div style={{marginTop: "2rem"}}>
            <UserList/>
          </div>
          </TabPane>

          {/* Displaying the TAB 3: List of all the reviews */}
          <TabPane tabId="3">
          <div style={{marginTop: "2rem"}}>
            <ReviewList/>
          </div>
          </TabPane>

          {/* Displaying the TAB 4: List of all the comments */}
          <TabPane tabId="4">
          <div style={{marginTop: "2rem"}}>
            <CommentList/>
          </div>
          </TabPane>

        </TabContent>
        </div> 
        ) : (
          <h4 className='mb-3 ml-4'>You're can't access to this page because you're not an admin.</h4>
        )}
      </div>
    ):(<h4 className='mb-3 ml-4'>You're can't access to this page because you're not an admin.</h4>)}; 
    </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
  auth: state.auth,
  university: state.university,
  review: state.review,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  {getUsers, deleteUser, getUniversities, deleteUniversity, getReviews, deleteReview, getComments, deleteComment}
)(AdminDashboard);
