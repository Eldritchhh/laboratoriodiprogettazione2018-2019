import React, { Component } from 'react';
import { Container, Jumbotron} from 'reactstrap';
import { connect } from 'react-redux';


class About extends Component {

  render() {
    return (
      <Jumbotron style={{ marginLeft: '1rem', marginRight: '1rem' }}>
        <Container fluid>
          <h3>Benvenuti in ErasmusGO!</h3>
          <p> Lo scopo principale del prodotto è fornire un servizio che consenta agli 
            studenti universitari (vincolante) che hanno intenzione di passare un ipotetico
            periodo di studi all’estero (non vincolante) di ottenere informazioni, consigli 
            e pareri personali sull’esperienza Erasmus in una determinata sede universitaria 
            d’Europa. Il mezzo che renderà possibile lo scambio informativo sarà la condivisione 
            di contenuti all’interno della comunità di studenti che hanno svolto un periodo di studi all’estero.</p>
        </Container>
      </Jumbotron>
    );
  }
}

export default connect(
)(About);
