import React, { Component } from 'react';
import AppNavbar from './components/AppNavbar';
import Home from './pageComponents/Home';
import About from './pageComponents/About';
import ReviewModal from './components/review/ReviewModal';
import UniversityGrid from './components/university/UniversityGrid';
import UniversityPage from './components/university/UniversityPage';

import { Provider } from 'react-redux';
import store from './store';
import { loadUser } from './actions/authActions';

import 'bootstrap/dist/css/bootstrap.min.css';  
import './App.css';
import { BrowserRouter, Route} from 'react-router-dom';
import AdminDashboard from './pageComponents/AdminDashboard';

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }

  render() {
    return (
      <BrowserRouter>
        <Provider store={store}>
          <div className='App'>
            <AppNavbar />

            < Route exact path='/' component = {Home} />
            < Route path='/university' component = {UniversityGrid} />
            < Route path='/about' component={About} />
            < Route path='/universityPage' component={UniversityPage} />
            < Route path='/adminDashboard' component={AdminDashboard} />
            < Route path='/reviewModal' component={ReviewModal} />

          </div>
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;
