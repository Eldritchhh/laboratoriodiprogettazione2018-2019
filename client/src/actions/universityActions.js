import axios from 'axios';
import { GET_UNIVERSITIES, ADD_UNIVERSITY, DELETE_UNIVERSITY, UNIVERSITIES_LOADING } from './types';
import { tokenConfig } from './authActions';
import { returnErrors } from './errorActions';

export const getUniversities = () => dispatch => {
  dispatch(setUniversitiesLoading());
  axios
    .get('/api/universities')
    .then(res =>
      dispatch({
        type: GET_UNIVERSITIES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const addUniversity = university => (dispatch, getState) => {
  axios
    .post('/api/universities', university)
    .then(res => 
      dispatch({
        type: ADD_UNIVERSITY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const deleteUniversity = id => (dispatch, getState) => {
  axios
    .delete(`/api/universities/${id}`, tokenConfig(getState))
    .then(res =>
      dispatch({
        type: DELETE_UNIVERSITY,
        payload: id
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const setUniversitiesLoading = () => {
  return {
    type: UNIVERSITIES_LOADING
  };
};
