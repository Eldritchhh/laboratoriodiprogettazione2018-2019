import axios from 'axios';
import { GET_COMMENTS, ADD_COMMENT, DELETE_COMMENT, COMMENTS_LOADING } from './types';
import { tokenConfig } from './authActions';
import { returnErrors } from './errorActions';

export const getComments = () => dispatch => {
  dispatch(setCommentsLoading());
  axios
    .get('/api/comments')
    .then(res =>
      dispatch({ 
        type: GET_COMMENTS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const addComment = comment => (dispatch, getState) => {
  axios
    .post('/api/comments', comment)
    .then(res => 
      dispatch({
        type: ADD_COMMENT,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const deleteComment = id => (dispatch, getState) => {
  axios
    .delete(`/api/comments/${id}`, tokenConfig(getState))
    .then(res =>
      dispatch({
        type: DELETE_COMMENT,
        payload: id
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const setCommentsLoading = () => {
  return {
    type: COMMENTS_LOADING
  };
};
