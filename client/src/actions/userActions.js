import axios from 'axios';
import { returnErrors } from './errorActions';
import { tokenConfig } from './authActions';

import {
    GET_USERS,
    DELETE_USER,
    USERS_LOADING,
} from './types';

export const getUsers = () => dispatch => {
    dispatch(setUsersLoading());
    axios
      .get('/api/users')
      .then(res =>
        dispatch({
          type: GET_USERS,
          payload: res.data
        })
      )
      .catch(err =>
        dispatch(returnErrors(err.response.data, err.response.status))
      );
  };
  
  
  export const deleteUser= id => (dispatch, getState) => {
    axios
      .delete(`/api/users/${id}`, tokenConfig(getState))
      .then(res =>
        dispatch({
          type: DELETE_USER,
          payload: id
        })
      )
      .catch(err =>
        dispatch(returnErrors(err.response.data, err.response.status))
      );
  };


  export const setUsersLoading = () => {
    return {
      type: USERS_LOADING
    };
  };
  