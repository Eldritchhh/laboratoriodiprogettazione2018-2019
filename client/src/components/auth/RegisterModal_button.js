import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Alert
} from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { register } from '../../actions/authActions';
import { clearErrors } from '../../actions/errorActions';
import Select from 'react-select';
import registerValidation from './../../validationForms/RegisterValidation.js';

class RegisterModal extends Component {
  state = {
    modal: false,
    name: '',
    email: '',
    department: '',
    password: '',
    admin: false,
    msg: null,
    errors: []
  };

  static propTypes = {
    isAuthenticated: PropTypes.bool,
    error: PropTypes.object.isRequired,
    register: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired
  };

  componentDidUpdate(prevProps) {
    const { error } = this.props;
    if (error !== prevProps.error) {
      // Check for register error
      if (error.id === 'REGISTER_FAIL') {
        this.setState({ msg: error.msg.msg });
      } else {
        this.setState({ msg: null });
      }
    }

  }

  toggle = () => {
    // Clear errors
    this.props.clearErrors();
    this.setState({
      modal: !this.state.modal
    });
  };

  onChange = e => {
    if(e.target.name === "admin") {
      if(e.target.value === "Admin") {
        this.setState({ [e.target.name]: true });
      }
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
  };

  onSubmit = e => {
    e.preventDefault();

    const { name, email, department, password, admin } = this.state;

    console.log(admin)
    // Create user object
    const newUser = {
      name,
      email,
      department,
      password,
      admin
    };

    // Check if there is an invalid field within the Register Form

    const errors = registerValidation(newUser);
    
    if (errors.length > 0) {
      this.setState({ errors });
      return;
    }
    else {
      // Attempt to register
      this.props.register(newUser);
    }
  };

  onChangeSelect = name => value => {
    if(value == null){
      this.setState({
        [name]: "",
      });
    }
    else {
      this.setState({
        [name]: value.value,
      });
    }
  };

  render() {
    const { errors } = this.state;

    const suggestions_department = [
      { label: 'DEPARTMENT OF BIOTECHNOLOGY AND BIOSCIENCES' },
      { label: 'DEPARTMENT OF ECONOMICS, MANAGEMENT AND STATISTICS' },
      { label: 'DEPARTMENT OF INFORMATICS, SYSTEMS AND COMMUNICATION (DISCO)' },
    ].map(suggestion => ({
      value: suggestion.label,
      label: suggestion.label,
    }));

    return (
      <div>
        {this.props.isAuthenticated ? (
          <Button
            color='dark'
            style={{ marginBottom: '2rem' }}
            onClick={this.toggle}
          >
            Add User
          </Button>
        ) : (
          <h4 className='mb-3 ml-4'>Please log in to manage user</h4>
        )}

        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Register</ModalHeader>
          <ModalBody>
            {this.state.msg ? (
              <Alert color='danger'>{this.state.msg}</Alert>
            ) : null}
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for='name'>Name</Label>
                <Input
                  type='text'
                  name='name'
                  id='name'
                  placeholder='Name'
                  className='mb-3'
                  onChange={this.onChange}
                />

                <Label for='email'>Email</Label>
                <Input
                  type='email'
                  name='email'
                  id='email'
                  placeholder='Email'
                  className='mb-3'
                  onChange={this.onChange}
                />
              
                <Label for='department'> Department </Label>
                <Select
                name='department'
                id='department'
                options={suggestions_department}
                onChange={this.onChangeSelect("department")}
                placeholder="Select a department"
                className='mb-3'
                isClearable
                />

                <Label for='password'>Password</Label>
                <Input
                  type='password'
                  name='password'
                  id='password'
                  placeholder='Password'
                  className='mb-3'
                  onChange={this.onChange}
                />

                <Label for='admin'>Admin</Label>
                <Input
                  type='admin'
                  name='admin'
                  id='admin'
                  placeholder='Type Admin if you want to create an admin user.'
                  className='mb-3'
                  onChange={this.onChange}
                />

                {errors.length > 0 ? (<Alert color='danger'> {errors.map(error => (<p key={error}>Error : {error}</p>))}</Alert>) : null}
                                 
                <Button color='dark' style={{ marginTop: '2rem' }} block>
                  Register
                </Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(
  mapStateToProps,
  { register, clearErrors }
)(RegisterModal);
