import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition } from 'react-transition-group';
import { connect } from 'react-redux';
import { getUsers, deleteUser } from '../../actions/userActions';
import PropTypes from 'prop-types';
import RegisterModal from './RegisterModal_button';

class UserList extends Component {
  static propTypes = {
    getUsers: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool
  };

  componentDidMount() {
    this.props.getUsers();
  }

  onDeleteClick = id => {
    this.props.deleteUser(id);
  };

  render() {
    const { users } = this.props.user;

    return (
      <Container>
        <RegisterModal />
        <ListGroup>
            {users.map(({ _id, name }) => (
              <CSSTransition key={_id} timeout={500} classNames='fade'>
                <ListGroupItem>
                  {this.props.isAuthenticated ? (
                    <Button
                      className='remove-btn'
                      color='danger'
                      size='sm'
                      onClick={this.onDeleteClick.bind(this, _id)}
                    >
                      &times;
                    </Button>
                  ) : null}
                  {name}
                </ListGroupItem>
              </CSSTransition>
            ))}
        </ListGroup>
      </Container>  
      )
    }
  }

const mapStateToProps = state => ({
  user: state.user,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  { getUsers, deleteUser }
)(UserList);
