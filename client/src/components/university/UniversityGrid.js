import React, { Component } from 'react';
import { Container, Input, Alert} from 'reactstrap';
import { connect } from 'react-redux';
import { getUniversities } from '../../actions/universityActions';
import PropTypes from 'prop-types';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import { Link} from 'react-router-dom';
import Select from 'react-select';
import universityValidation from './../../validationForms/UniversityValidation'
import test_img from '../../media/placeholder.png';
import unimi from '../../media/unimi.jpg';
import unimib from '../../media/unimib.jpg';
import amsterdam from '../../media/amsterdam.jpg';
import tromso from '../../media/tromso.jpg';

class universityGrid extends Component {
  
  state = {
    searchByName : "",
    searchByCountry : "",
    searchByCity : "",
    universities_number : 0,
    errors : []
  }

  static propTypes = {
    getUniversities: PropTypes.func.isRequired,
    university: PropTypes.object.isRequired,
  };

  filterListName = event => {
    this.setState({ searchByName : event.target.value })
  }

  componentDidMount() {
    this.props.getUniversities()
  };

  componentDidUpdate() {
    console.log(this)
    var errors_temp 
    errors_temp = universityValidation(this.state.searchByName, this.state.searchByCountry, this.state.searchByCity, this.state.universities_number)
    if (this.state.errors.length > 0) {
      this.setState({ errors : errors_temp });
      return;
    }
  };

  handleChange = name => value => {

    if(value == null){
      this.setState({
        [name]: "",
      });
    }
    else {
      this.setState({
        [name]: value.value,
      });
    }
  };
  
  renderSwitch(param) {
    switch(param) {
      case "University of Milano Statale":
        return <img src={unimi} alt={test_img}/>;
      case "University of Milano Bicocca":
        return <img src={unimib} alt={test_img}/>;
      case "University of Amsterdam":
        return <img src={amsterdam} alt={test_img}/>;
      case "University of Tromsø":
        return <img src={tromso} alt={test_img}/>;
      default:
        return <img src={test_img} alt={test_img}/>;
    }
  }

  render() {
    var { universities } = this.props.university;
    var universities_number
    var errors = this.state.errors
    const isMobile = window.innerWidth <= 500;
    var s_country 
    var s_cities

    switch(this.state.searchByCity) {
      case "Milan":
          s_country = [{ label: 'Italy' }]
          break;
      case "Amsterdam":
          s_country = [{ label: 'Netherlands' }]
          break;
      case "Tromsø":
          s_country = [{ label: 'Norway' }]
          break;
      default:
          s_country = [      
            { label: 'Italy' },
            { label: 'Netherlands'},
            { label: 'Norway' },]
    }

    const suggestions_country = s_country.map(suggestion => ({
      value: suggestion.label,
      label: suggestion.label,
    }));

    switch(this.state.searchByCountry) {
      case "Italy":
          s_cities = [{ label: 'Milan' }]
          break;
      case "Netherlands":
          s_cities = [{ label: 'Amsterdam' }]
          break;
      case "Norway":
          s_cities = [{ label: 'Tromsø' }]
          break;
      default:
          s_cities = [
            { label: 'Milan' },
            { label: 'Amsterdam' },
            { label: 'Tromsø' }]
    }

    const suggestions_cities = s_cities.map(suggestion => ({
      value: suggestion.label,
      label: suggestion.label,
    }));
    
    return (

      <Container>
        University name:
        <Input type="text" placeholder="Search name"  maxLength="60" onChange={this.filterListName} style={{ marginBottom: '1rem' }}/>
        University country:
        <Select
            options={suggestions_country}
            onChange={this.handleChange('searchByCountry')}
            placeholder="Search a country"
            isClearable
          />
        <div style={{ marginTop: '1rem' }}> University city: </div>
        <Select
          options={suggestions_cities}
          onChange={this.handleChange('searchByCity')}
          placeholder="Search a city"
          isClearable
          
        />

        {errors.length > 0 ? (<Alert color='danger'> {errors.map(error => (<p key={error}>Error : {error}</p>))}</Alert>) : null}

        <div style={{ marginTop: '2rem' }}> Number of results : { universities_number = universities.filter(univ => 
                  univ.name.toLowerCase().includes((this.state.searchByName).toLowerCase()) &&
                  univ.country.toLowerCase().includes((this.state.searchByCountry).toLowerCase()) &&
                  univ.city.toLowerCase().includes((this.state.searchByCity).toLowerCase())
                  ).length} </div>

        { universities_number === 0 ?  (<Alert style={{marginTop: '1rem', marginBottom: '1rem'}} color='secondary'> There are no universities that match your search!  </Alert>) : 
        
          < GridList cellheight={180} className={universities.name} cols={isMobile ? 1 : 2} >
                <GridListTile key="Subheader" cols = {2} style={{ height: 'auto' }} rows= {2}/>

                {universities.filter(univ => 
                      univ.name.toLowerCase().includes((this.state.searchByName).toLowerCase()) &&
                      univ.country.toLowerCase().includes((this.state.searchByCountry).toLowerCase()) &&
                      univ.city.toLowerCase().includes((this.state.searchByCity).toLowerCase())
                  )
                  
                  .map((universities) => (
                    
                  <GridListTile key={universities._id}>

                  {this.renderSwitch(universities.name)}  

                    <GridListTileBar
                      cellheight={180}
                      title={<div style={{color: "white"}}> <h6> {universities.name} </h6> </div>}
                      subtitle={<span> {universities.country}, {universities.city} | {universities.website} </span>}
                      actionIcon={
                        <Link to={{
                          pathname: '/universityPage',
                          state: {university_id: universities._id}
                        }} style={{color: "white"}}>

                        <IconButton > 
                        {<div style={{color: "white"}}> <h5> Show More </h5> </div>}
                        </IconButton>
                        </Link>
                      }
                    />
                  </GridListTile>
                ))}
              </GridList>
          }
      </Container>  
    )}
}

const mapStateToProps = state => ({
  university: state.university
});

export default connect(
  mapStateToProps,
  { getUniversities }
)(universityGrid);
