import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition } from 'react-transition-group';
import { connect } from 'react-redux';
import { getUniversities, deleteUniversity } from '../../actions/universityActions';
import PropTypes from 'prop-types';
import UniversityModal from './UniversityModal';

class UniversityList extends Component {
  static propTypes = {
    getUniversities: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool
  }; 
 
  componentDidMount() {
    this.props.getUniversities();
  }

  onDeleteClick = id => {
    this.props.deleteUniversity(id);
  };

  render() {
    const { universities } = this.props.university;
    console.log(universities)

    return (
      <Container>
        <UniversityModal />

        <ListGroup>
            {universities.map(({ _id, name }) => (
              <CSSTransition key={_id} timeout={500} classNames='fade'>
                <ListGroupItem>
                  {this.props.isAuthenticated ? (
                    <Button
                      className='remove-btn'
                      color='danger'
                      size='sm'
                      onClick={this.onDeleteClick.bind(this, _id)}
                    >
                      &times;
                    </Button>
                  ) : null}
                  {name}
                </ListGroupItem>
              </CSSTransition>
            ))}
        </ListGroup>
      </Container>  
    );
  }
}

const mapStateToProps = state => ({
  university: state.university,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  { getUniversities, deleteUniversity }
)(UniversityList);
