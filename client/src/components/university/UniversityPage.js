import React, { Component} from 'react';
import {Table, Container, Button, TabContent, TabPane, 
        Nav, NavItem, NavLink, CardTitle, Card, CardText, Collapse,
        Modal,
        ModalHeader,
        ModalBody,   
        Form,
        FormGroup,
        Label,
        Alert,
        Input,} from 'reactstrap';
import classnames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';

import { getUniversities } from '../../actions/universityActions';
import { getReviews, deleteReview } from '../../actions/reviewActions';
import { getUsers } from '../../actions/userActions';
import { format } from 'date-fns';
import averageRatings from './../review/StatFunctions.js';
import { getComments, addComment, deleteComment } from '../../actions/commentActions';
import commentValidation from './../../validationForms/CommentValidation';

class UniversityPage extends Component {

  static propTypes = {
    getUsers: PropTypes.func.isRequired,
    getUniversities: PropTypes.func.isRequired,
    getReviews: PropTypes.func.isRequired,
    getComments: PropTypes.func.isRequired,
    university: PropTypes.object.isRequired,
    comment: PropTypes.object.isRequired,
    review: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool,
    userId: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.toggleUniversity = this.toggleUniversity.bind(this);
    this.state = { collapseUniversity: false };

    this.toggle = this.toggle.bind(this);
    this.state = {
      commentModal: false,
      reviewing: "",
      content: "",
      activeTab: '1',
      errors: []
    };
  }

  onLoginSuccess() {

  }
    componentDidMount() {
    this.props.getComments()
    this.props.getUsers()
    this.props.getUniversities()
    this.props.getReviews()
  };
  
  toggleUniversity() {
    this.setState(state => ({ collapseUniversity: !state.collapseUniversity }));
  }

  toggleComment() {
    this.setState({ commentModal: false });
  }
  
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  toggleComment = (review_id) => {
    console.log('this is:', this);
    this.setState({
      commentModal: !this.state.commentModal,
      reviewing: review_id
    });
  };

  onSubmit = e => {
    e.preventDefault()
    
    const newComment = {
      id_user: this.props.userId.name,
      id_user_cod: this.props.userId._id,
      id_review: this.state.reviewing,
      content: this.state.content
    };

    // Check if there is an invalid field within the Comment Form
    const errors = commentValidation(newComment);

    console.log(errors)
    if (errors.length > 0) {
      this.setState({ errors });
      return;
    }
    else {
      // Attempt to add comment
      this.props.addComment(newComment);
      // Close the comment modal
      this.setState({ commentModal: false });
    }

  };

  onDeleteClickComment = id => {
    this.props.deleteComment(id);
  };

  onDeleteClickReview = id => {
    this.props.deleteReview(id);
  };


  render() {
    const { universities } = this.props.university;
    const { reviews } = this.props.review;
    const { users } = this.props.user;
    const { comments } = this.props.comment;
    const university_id = this.props.location.state.university_id;
    const { errors } = this.state;
    console.log(this)
    
    return (
      <div>
      {/* Displaying the university name */}
        {universities.filter(x => x._id === university_id).map((universities) => (
                  <Typography variant="h2" gutterBottom style={{ padding: '1rem'}}>
                    {universities.name}
                  </Typography>
            ))}

        {/* Displaying the button for create a new review */}
        {this.props.isAuthenticated ? (
              <Link to={{pathname: '/reviewModal', state: {university_id: university_id, user_id: this.props.userId._id}}} style={{color: "white"}}>
                <Button style={{ marginTop: '2rem', marginBottom: '2rem'  }} block>
                    Write a review! 
                </Button>
              </Link>

            ) : (
              <Typography variant="h5" gutterBottom style={{marginLeft: '1rem', marginBottom: '1rem' }} >
                 Please log in to write a review!
              </Typography>
          )}

        {/* Displaying the navigation bar and the available tabs 
            Tab 1: Description of the university
            Tab 2: List of the reviews
            Tab 3: Statistics about the university */}
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              Description
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              Reviews
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}
            >
              Tips
            </NavLink>
          </NavItem>
        </Nav>

       {/* Displaying the TAB 1: Description of the university */}
       <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
          <div style={{marginTop: "1rem"}}>
            {universities.filter(x => x._id === university_id).map((universities) => (
            <Container fluid style={{ padding: "1rem"}}>
            <Table hover>
                  <tbody>
                    <tr>
                      <th scope="row">Country</th>
                      <td>{universities.country}  </td>
                    </tr>
                    <tr>
                      <th scope="row">City</th>
                      <td>{universities.city}  </td>
                    </tr>
                    <tr>
                      <th scope="row">Website</th>
                      <td>{universities.website} </td>
                    </tr>
                    <tr>
                      <th scope="row">Address</th>
                      <td>{universities.address} </td>
                    </tr>
                    <tr>
                      <th scope="row">Telephone</th>
                      <td>{universities.telephone} </td>
                    </tr>
                    <tr>
                      <th scope="row">Email</th>
                      <td>{universities.email} </td>
                    </tr>
                    <tr>
                      <th scope="row">EmailDomain</th>
                      <td>{universities.emailDomain} </td>
                    </tr>
                  </tbody>
                </Table>
                </Container>
            ))}
          </div>
          </TabPane>

          {/* Displaying the TAB 2: List of the reviews */}
          <TabPane tabId="2">
            <div style={{marginTop: "1rem"}}>
              <Container fluid style={{ padding: "2rem"}}>
                    {reviews.filter(x => x.id_university_reviewed === university_id).map((reviews) => (
                    <div> 

                    <Card body outline color="success" style={{marginTop: "2rem", padding: "1rem"}}>
                      <CardTitle>
                        {this.props.isAuthenticated && this.props.userId !== undefined ? [(this.props.userId._id === reviews.id_reviewer || this.props.userId.admin === true) ? 
                        (<Button className='remove-btn' color='danger'size='sm' 
                          onClick={this.onDeleteClickReview.bind(this, reviews._id)}> &times; </Button>) 
                          : console.log(this.props.userId._id, reviews.id_reviewer)] : console.log(this.props)}
                        <Typography variant="h5" gutterBottom>
                          Made by <b> {users.filter(u => u._id === reviews.id_reviewer).map((users) => (users.name))} </b> 
                          on <b> {format(reviews.date, 'MMMM Do, YYYY')}</b> 
                        </Typography>
                      </CardTitle> 
                      <CardText> 
                        <p> <b> Mobility period:</b> {format(reviews.startMobility, 'MMMM Do, YYYY')} - {format(reviews.endMobility, 'MMMM Do, YYYY')} </p>
                      </CardText>
                      <CardText> 
                      <h5> <b> Overall rating: </b> {parseFloat(averageRatings(reviews)[0].toFixed(2))} </h5>
                        <Table style={{ marginTop: "1rem"}}>
                        <tbody>
                          <tr>
                            <th scope="row"> University </th>
                            <td>{parseFloat(averageRatings(reviews)[1].toFixed(2))}</td>
                          </tr>
                          <tr>
                            <th scope="row">Faculty</th>
                            <td>{parseFloat(averageRatings(reviews)[2].toFixed(2))}</td>
                          </tr>
                          <tr>
                            <th scope="row">City</th>
                            <td>{parseFloat(averageRatings(reviews)[3].toFixed(2))}</td>
                          </tr>
                          <tr>
                          <th scope="row">Housing</th>
                            <td>{parseFloat(averageRatings(reviews)[4].toFixed(2))}</td>
                          </tr>
                        </tbody>
                      </Table>
                      </CardText>


                      <div>
                      {this.props.isAuthenticated ? (
                          <Button
                            color='dark'
                            style={{ marginBottom: '2rem' }}
                            onClick={(e) => this.toggleComment(reviews._id, e)}
                          >
                            Write comment
                          </Button>
                        ) : (
                          <h4 className='mb-3 ml-4'>Please log in to comment</h4>
                        )}
                        <Modal isOpen={this.state.commentModal} toggle={this.toggleComment}>
                          <ModalHeader toggle={this.toggleComment}> Write a comment </ModalHeader>
                          <ModalBody>
                          <Form onSubmit={this.onSubmit}>
                            <FormGroup>
                            <Label for='content' style={{ marginTop: '1rem' }}>Content of the comment</Label>
                              <Input
                                type='content'
                                name='content'
                                id='content'
                                placeholder='Add content of the comment'
                                onChange={this.onChange}
                              />
                             </FormGroup>
                              <Container fluid style={{ marginTop: '2rem', marginBottom: '2rem'  }} > 
                              </Container>
                              {errors.length > 0 ? (<Alert color='danger'> {errors.map(error => (<p key={error}>Error : {error}</p>))}</Alert>) : null}
                              <Button color='dark' style={{ marginTop: '2rem' }} block>
                                      Send the comment
                              </Button>
                              </Form>
                          </ModalBody>
                         </Modal>

                         </div>
                      <Button color="secondary" onClick={this.toggleUniversity} style={{ marginTop: '1rem', marginBottom: '1rem' }}> Show the comments.. </Button>
                      <Collapse isOpen={this.state.collapseUniversity}>
                        
                      <div>
                      {console.log(this)}
                      {comments !== undefined ? 
                        (comments.filter(comm => comm.id_review === reviews._id).map((comments) => (
                          <Card body outline color="secondary" style={{marginTop: "2rem", padding: "1rem"}}>
                          <CardTitle>
                          {this.props.isAuthenticated && this.props.userId !== undefined ? [(this.props.userId._id === comments.id_user_cod || this.props.userId.admin === true) ? 
                          (<Button className='remove-btn' color='danger'size='sm' 
                            onClick={this.onDeleteClickComment.bind(this, comments._id)}> &times; </Button>) 
                            : null] : null}
                            <Typography variant="h6" gutterBottom>
                              Made by <b> {comments.id_user} </b> 
                              on <b> {format(comments.date, 'MMMM Do, YYYY')}</b> 
                            </Typography>
                          </CardTitle> 
                          <CardText> 
                            <p> {comments.content} </p>
                          </CardText>
                        </Card>
                    
                        ))) :
                        ("")
                      }
                      </div>
                      </Collapse>

                      </Card>
                      </div>
                    ))}
              </Container>
            </div>
          </TabPane>

         {/* Displaying the TAB 3: Statistics about the university*/}
          <TabPane tabId="3">
            <div style={{marginTop: "2rem"}}>
              <Container fluid style={{ padding: "2rem"}}>
                Suggestions
              </Container>
            </div>
          </TabPane>

        </TabContent>
        
      </div>

    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  review: state.review,
  comment: state.comment,
  university: state.university,
  isAuthenticated: state.auth.isAuthenticated,
  userId: state.auth.user

});

export default connect(
  mapStateToProps,
  { getUniversities, getReviews, getUsers, getComments, addComment, deleteReview, deleteComment}
)(UniversityPage);
