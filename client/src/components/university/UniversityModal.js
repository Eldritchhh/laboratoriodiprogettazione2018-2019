import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import { connect } from 'react-redux';
import { addUniversity } from '../../actions/universityActions';
import PropTypes from 'prop-types';

class UniversityModal extends Component {
  state = {
    modal: false,
    name: ''
  };

  static propTypes = {
    isAuthenticated: PropTypes.bool
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newUniversity = {
      name: this.state.name,
      country: this.state.country,
      city: this.state.city,
      address: this.state.address,
      telephone: this.state.telephone,
      website: this.state.website,
      email: this.state.email,
      emailDomain: this.state.emailDomain
    };

    // Add item via addUniversity action
    this.props.addUniversity(newUniversity);

    // Close modal
    this.toggle();
  };

  render() {
    return (
      <div>
        {this.props.isAuthenticated ? (
          <Button
            color='dark'
            style={{ marginBottom: '2rem' }}
            onClick={this.toggle}
          >
            Add University
          </Button>
        ) : (
          <h4 className='mb-3 ml-4'>Please log in to manage universities</h4>
        )}

        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Add To University List</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for='univ_name'>Name</Label>
                <Input
                  type='text'
                  name='name'
                  id='univ_name'
                  placeholder='Add university name'
                  onChange={this.onChange}
                />
              
                <Label for='univ_country' style={{ marginTop: '1rem' }}>Country</Label>
                <Input
                  type='text'
                  name='country'
                  id='univ_country'
                  placeholder='Add university country'
                  onChange={this.onChange}
                />

                
                <Label for='city' style={{ marginTop: '1rem' }}>City</Label>
                <Input
                  type='text'
                  name='city'
                  id='univ_city'
                  placeholder='Add university city'
                  onChange={this.onChange}
                />

                
                <Label for='address' style={{ marginTop: '1rem' }}>Address</Label>
                <Input
                  type='text'
                  name='address'
                  id='univ_address'
                  placeholder='Add university address'
                  onChange={this.onChange}
                />

                <Label for='telephone' style={{ marginTop: '1rem' }}>Telephone</Label>
                <Input
                  type='text'
                  name='telephone'
                  id='univ_telephone'
                  placeholder='Add university telephone'
                  onChange={this.onChange}
                />

                <Label for='website' style={{ marginTop: '1rem' }}>Website</Label>
                <Input
                  type='text'
                  name='website'
                  id='univ_website'
                  placeholder='Add university website'
                  onChange={this.onChange}
                />

                <Label for='email' style={{ marginTop: '1rem' }}>Email</Label>
                <Input
                  type='text'
                  name='email'
                  id='univ_email'
                  placeholder='Add university email'
                  onChange={this.onChange}
                /> 

                <Label for='emailDomain' style={{ marginTop: '1rem' }}>Email Domain</Label>
                <Input
                  type='text'
                  name='emailDomain'
                  id='univ_emailDomain'
                  placeholder='Add university emailDomain'
                  onChange={this.onChange}
                />                  
                <Button color='dark' style={{ marginTop: '2rem' }} block>
                  Add University
                </Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  university: state.university,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  { addUniversity }
)(UniversityModal);
