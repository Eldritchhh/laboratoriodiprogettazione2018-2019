
function averageRatings(newReview) {

    const { university_Q1, university_Q2, university_Q3, university_Q4} = newReview;
    const { faculty_Q1, faculty_Q2, faculty_Q3 } = newReview;
    const { city_Q1, city_Q2, city_Q3, city_Q4} = newReview;
    const { housing_Q1, housing_Q2, housing_Q3, housing_Q4, housing_Q6} = newReview;
    // const { startMobility, endMobility, university_QE, faculty_QE, city_QE, housing_QE} = newReview;

    const avg_university = (parseInt(university_Q1) + parseInt(university_Q2) + parseInt(university_Q3) + parseInt(university_Q4))/4
    const avg_faculty = (parseInt(faculty_Q1) + parseInt(faculty_Q2) + parseInt(faculty_Q3))/3
    const avg_city = (parseInt(city_Q1) + parseInt(city_Q2) + parseInt(city_Q3) + parseInt(city_Q4))/4
    var avg_housing = 0
    
    if(housing_Q2 === "Residenza universitaria")
    {avg_housing = (parseInt(housing_Q1) + parseInt(housing_Q3) + parseInt(housing_Q4))/4} 
    else 
    {avg_housing = (parseInt(housing_Q1) + parseInt(housing_Q3) + parseInt(housing_Q6))/3}

    const tot_avg = (avg_university + avg_faculty + avg_city + avg_housing)/4
 
    return [tot_avg, avg_university, avg_faculty, avg_city, avg_housing];
}

module.exports = averageRatings;
