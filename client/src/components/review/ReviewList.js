import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition } from 'react-transition-group';
import { connect } from 'react-redux';
import { getReviews, deleteReview } from '../../actions/reviewActions';
import PropTypes from 'prop-types';

class ReviewList extends Component {
  static propTypes = {
    getReviews: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool
  };

  componentDidMount() {
    this.props.getReviews();
  }

  onDeleteClick = id => {
    this.props.deleteReview(id);
  };

  render() {
    const { reviews } = this.props.review;

    return (
      <Container>
        <ListGroup>
            {reviews.map(({ _id, id_reviewer, id_university_reviewed}) => (
              <CSSTransition key={_id} timeout={500} classNames='fade'>
                <ListGroupItem>
                  {this.props.isAuthenticated ? (
                    <Button
                      className='remove-btn'
                      color='danger'
                      size='sm'
                      onClick={this.onDeleteClick.bind(this, _id)}
                    >
                      &times;
                    </Button>
                  ) : null}
                { "ID Reviewer: " + id_reviewer + " @ ID University Reviewed: " + id_university_reviewed}
                </ListGroupItem>
              </CSSTransition>
            ))}
        </ListGroup>
      </Container>  
      )
    }
  }

const mapStateToProps = state => ({
  review: state.review,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  { getReviews, deleteReview }
)(ReviewList);
