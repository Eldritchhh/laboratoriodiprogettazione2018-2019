import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition } from 'react-transition-group';
import { connect } from 'react-redux';
import { getComments, deleteComment } from '../../actions/commentActions';
import PropTypes from 'prop-types';

class CommentList extends Component {
  static propTypes = {
    getComments: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool
  };

  componentDidMount() {
    this.props.getComments();
  }

  onDeleteClick = id => {
    this.props.deleteComment(id);
  };

  render() {
    const { comments } = this.props.comment;

    return (
      <Container>
        <ListGroup>
            {comments.map(({ _id, id_user, id_review, content}) => (
              <CSSTransition key={_id} timeout={500} classNames='fade'>
                <ListGroupItem>
                  {this.props.isAuthenticated ? (
                    <Button
                      className='remove-btn'
                      color='danger'
                      size='sm'
                      onClick={this.onDeleteClick.bind(this, _id)}
                    >
                      &times;
                    </Button>
                  ) : null}
                {"ID User: " + id_user + " @ ID University Reviewed: " + id_review + " ---> Content: " + content}
                </ListGroupItem>
              </CSSTransition>
            ))}
        </ListGroup>
      </Container>  
      )
    }
  }

const mapStateToProps = state => ({
  comment: state.comment,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  { getComments, deleteComment }
)(CommentList);
