
function commentValidation(newComment) {

    // we are going to store errors for all fields
    // in a signle array
    const errors = [];
    console.log("Validating the review form..")

    const {content} = newComment;
    // const {id_user, id_review} = newComment;

    if (content === "") {
        errors.push("You must enter a content for the current comment!");
    }
    return errors;
}

module.exports = commentValidation;
