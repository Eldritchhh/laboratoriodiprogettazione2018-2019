function universityValidation(searchByName, searchByCountry, searchByCity, universities_number) {

    // we are going to store errors for all fields
    // in a signle array
    const errors = [];
    console.log("Validating the university form..")

    if (searchByName.length > 60) {
        errors.push("Unviersity name cannot be longer than 60 chars!");
    }
    return errors;
}

module.exports = universityValidation;
