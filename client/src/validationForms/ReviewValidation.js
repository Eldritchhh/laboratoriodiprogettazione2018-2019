 
function monthDiff(d1, d2) {
    var months;
    
    var date1 = new Date(d1);
    var date2 = new Date(d2);

    months = (date2.getFullYear() - date1.getFullYear()) * 12;
    months -= date1.getMonth() + 1;
    months += date2.getMonth();
    return months <= 0 ? 0 : months;
}

function formValidation(newReview) {

    // we are going to store errors for all fields
    // in a signle array
    const errors = [];
    console.log("Validating the review form..")
    const {id_reviewer} = newReview;
    const {startMobility, endMobility} = newReview;
    console.log(startMobility, endMobility, monthDiff(startMobility,endMobility))
    const { university_Q1, university_Q2, university_Q3, university_Q4} = newReview;
    const { faculty_Q1, faculty_Q2, faculty_Q3 } = newReview;
    const { city_Q1, city_Q2, city_Q3, city_Q4} = newReview;
    const { housing_Q1, housing_Q2, housing_Q3, housing_Q4, housing_Q6} = newReview;
    // const { housing_Q4, housing_Q5, housing_Q6, university_QE, faculty_QE, city_QE, housing_QE} = newReview;

    if(id_reviewer === null) {
        errors.push("Impossible to identify the reviewer's id");
    }

    if (startMobility === "toFill" || endMobility === "toFill") {
        errors.push("Please enter the starting and the ending date of your mobility");
    } else {
        if (startMobility > endMobility) {
            errors.push("The ending date can't be before the starting date of your mobility");
        } else {
            if (Date.now < startMobility || Date.now < endMobility ) {
                errors.push("The starting and ending date of your mobility have to be before the current date");
            } else {
                if (monthDiff(startMobility,endMobility) <= 2) {
                    errors.push("A valid mobility period can't be shorter than 3 month. Please enter valid mobility dates");
                }
                if (monthDiff(startMobility,endMobility) > 12) {
                    errors.push("A valid mobility period can't be longer than 12 month. Please enter valid mobility dates");
                }
            }
        }
    }
    if (university_Q1 === 0 || university_Q2 === 0 || university_Q3 === 0 || university_Q4 === 0) {
        errors.push("Please enter all the 'University' fields");
    }
    if (faculty_Q1 === 0 || faculty_Q2 === 0 || faculty_Q3 === 0) {
        errors.push("Please enter all the 'Faculty' fields");
    }
    if (city_Q1 === 0 || city_Q2 === 0 || city_Q3 === 0 || city_Q4 === 0) {
        errors.push("Please enter all the 'City' fields");
    }
    
    // Conditional questions are not required at the moment
    if ( (housing_Q1 === 0 || housing_Q2 === 0 || housing_Q3 === 0) || (housing_Q2 === "Residenza universitaria" && housing_Q4 === 0 ) || (housing_Q2 === "Residenza privata" && housing_Q6 === 0 )) {
        errors.push("Please enter all the 'Housing' fields");
    }

    return errors;
}

module.exports = formValidation;
