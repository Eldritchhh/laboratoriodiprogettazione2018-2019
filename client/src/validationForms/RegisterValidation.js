 function registerValidation(newUser) {

    const errors = []
    const {name, email, department, password} = newUser;
    var regex = /^[a-zA-Z0-9 ]*$/

    // Test #1, #3
    if(name === "" || email === "" || department === "" || password === "") {
        errors.push("Please enter all the above fields!")
    } else {

        // Test #2
        if(password.length < 8 ) {
            errors.push("Password has to be at least of 8 chars!")
        }
    
        // Test #4
        if(!email.toLowerCase().endsWith("@campus.unimib.it")) {
            errors.push("Email must belong to Unimib email domain!")
        }

        // Test #5
        if(false) {
            errors.push("User already exists!")
        }

        // Test #6
        if(!email.toLowerCase().endsWith("@campus.unimib.it")) {
            errors.push("Email field enter is not valid!")
        }

        // Test #7
        if(regex.test(name) === false) {
            errors.push("User name field cannot contains special chars!")
        }

        // Test #8
        if(regex.test(password) === true) {
            errors.push("Password field must contains at least 1 special char!")
        }
        
        return errors;
    }

    return errors;
}

module.exports = registerValidation;
