import {
    GET_UNIVERSITIES,
    ADD_UNIVERSITY,
    DELETE_UNIVERSITY,
    UNIVERSITIES_LOADING
  } from '../actions/types';
  
  const initialState = {
    universities: [],
    loading: false
  };
  
  export default function(state = initialState, action) {
    switch (action.type) {
      case GET_UNIVERSITIES:
        return {
          ...state,
          universities: action.payload,
          loading: false
        };
      case DELETE_UNIVERSITY:
        return {
          ...state,
          universities: state.universities.filter(university => university._id !== action.payload)
        };
      case ADD_UNIVERSITY:
        return {
          ...state,
          universities: [action.payload, ...state.universities]
        };
      case UNIVERSITIES_LOADING:
        return {
          ...state,
          loading: true
        };
      default:
        return state;
    }
  }
  