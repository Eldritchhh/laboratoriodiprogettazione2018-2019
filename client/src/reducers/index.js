import { combineReducers } from 'redux';
import reviewReducer from './reviewReducer';
import errorReducer from './errorReducer';
import authReducer from './authReducer';
import universityReducer from './universityReducer';
import userReducer from './userReducer';
import commentReducer from './commentReducer';

export default combineReducers({
  review: reviewReducer,
  user: userReducer,
  error: errorReducer,
  auth: authReducer,
  university: universityReducer,
  comment: commentReducer
});
